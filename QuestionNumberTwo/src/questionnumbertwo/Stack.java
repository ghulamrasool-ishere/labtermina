/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionnumbertwo;

/**
 *
 * @author G A M A
 */
public class Stack {
    private String str[];
    private int top;
    private int capacity;
    
    Stack(int size){
        str = new String[size];
        capacity = size;
        top =-1;
    }
    
    public boolean isEmpty(){
        return top==-1;
    }
    
    public boolean isFull(){
        return top==capacity-1;
    }
    
    public void push(String a){
        if (isFull()){
            System.out.println("Stack is Full");
        }
        else{
            str[++top] = a;
        }   
    }
    
    public String pop (){
        if (isEmpty()){
            System.out.println("Stack is empty");
            return "Stack is empty";
        }
        else {
            return str[top--];
        }
    }
    
    public String peek(){
        if (!isEmpty()){
            return str[top];
        }
        return "Stack Empty";
    }
    
    public String toString() {
    String result = "";

    for (int i = 0; i <= top; i++)
        result = result + str[i] + " ";
    return result;
}
}