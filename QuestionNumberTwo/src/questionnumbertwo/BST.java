/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionnumbertwo;

/**
 *
 * @author G A M A
 */
public class BST {
    Node root,temp;
    
    BST() {  
        root = null;  
    } 
    
    public void create_node (FrameQueue data, int clip_no){
        temp = new Node(clip_no, data);
    }
    
    void insert (FrameQueue data, int clip_no){
        temp = root;
        create_node(data, clip_no);
        if (root == null) { 
            root = this.temp; 
        } 
        while(true){
            if (clip_no < temp.getClip_No()) {
                if(temp.left==null){
                    root.left = this.temp; 
                    break;
                }
                temp = temp.left;
            }
            else {
                if(temp.right==null){
                    root.right = this.temp; 
                    break;
                }
                temp = temp.right;
            }
        }
    }
    
    public Node minFrame(){
        temp = root;
        while(temp.left!=null){
            temp=temp.left;
        }
        System.out.println("Minimum Frame (Clip No) : \n " + temp.getClip_No());
        return temp;
    }
    
    public void inOrder(Node r){
        if (r!= null) { 
            inOrder(r.left); 
            System.out.println(r.toString() + "\n\n"); 
            inOrder(r.right); 
        } 
    }
}
