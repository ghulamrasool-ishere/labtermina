/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionnumbertwo;

/**
 *
 * @author G A M A
 */
public class Node {
    private int clip_No;
    FrameQueue data;
    Node left,right;

    public Node(int clip_No, FrameQueue data) {
        this.clip_No = clip_No;
        this.data = data;
        left = null;
        right=null;
    }
    public int getClip_No() {
        return clip_No;
    }

    public FrameQueue getData() {
        return data;
    }

    public void setClip_No(int clip_No) {
        this.clip_No = clip_No;
    }

    public void setData(FrameQueue data) {
        this.data = data;
    }
    public String toString(){
        String a = "";
        a = "Clip No : " + clip_No + "\nData:\n" + data.toString();
        return a;
    }
}
