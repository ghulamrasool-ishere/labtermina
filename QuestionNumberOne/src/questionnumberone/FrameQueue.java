/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionnumberone;

/**
 *
 * @author G A M A
 */
public class FrameQueue {
    private int front, rear, capacity; 
    private Frame queue[]; 
    
    FrameQueue(int c){ 
        front = rear = 0; 
        capacity = c; 
        queue = new Frame[capacity]; 
        for (int i = 0;i<capacity;i++){
           queue[i] =  new Frame();
        }
    } 
    
    public void enqueue (Frame f){
        if (capacity == rear) { 
            System.out.println("Queue is full"); 
            return; 
        }
        else { 
            queue[rear] = f; 
            rear++; 
        } 
    }
    
    public Frame dequeue(){
        Frame temp = null;
        if (front == rear) { 
            System.out.printf("\nQueue is empty\n"); 
            return null; 
        } 
        else { 
            
            temp = queue[front];
            queue[front]=null;
            front++;
        } 
        return temp;
    }
    
    public boolean isEmpty(){
        if (front == rear) { 
            System.out.printf("\nQueue is Empty\n"); 
            return true; 
        } 
        return false;
    }
    
    public boolean isFull (){
        if (capacity == rear) { 
            System.out.println("Queue is full"); 
            return true; 
        }
        return false;
    }
    
    public String toString(){
        int i; 
        String a="";
        if (front == rear) { 
            System.out.printf("\nQueue is Empty\n"); 
            return null; 
        } 
        for (i=front; i <rear; i++){ 
            a= a + queue[i].toString() + "\n";
        } 
        return a; 
    }

}

