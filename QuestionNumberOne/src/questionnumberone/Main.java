/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionnumberone;

/**
 *
 * @author G A M A
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        Frame f = new Frame();
        f.setFrame_No(0);
        f.setFrameData("string 1");
        f.setFrameData("string 2");
        f.setFrameData("string 3");
        f.setFrameData("string 4");
        f.setFrameData("string 5");
        f.setFrameData("string 6");
        f.setFrameData("string 7");
        f.setFrameData("string 8");
        f.setFrameData("string 9");
        f.setFrameData("string 10");
        
        
        Frame g = new Frame();
        g.setFrame_No(1);
        g.setFrameData("string 1");
        g.setFrameData("string 2");
        g.setFrameData("string 3");
        g.setFrameData("string 4");
        g.setFrameData("string 5");
        g.setFrameData("string 6");
        g.setFrameData("string 7");
        g.setFrameData("string 8");
        g.setFrameData("string 9");
        g.setFrameData("string 10");
        
        Frame h = new Frame();
        h.setFrame_No(2);
        h.setFrameData("string 1");
        h.setFrameData("string 2");
        h.setFrameData("string 3");
        h.setFrameData("string 4");
        h.setFrameData("string 5");
        h.setFrameData("string 6");
        h.setFrameData("string 7");
        h.setFrameData("string 8");
        h.setFrameData("string 9");
        h.setFrameData("string 10");
       
        Frame i = new Frame();
        i.setFrame_No(3);
        i.setFrameData("string 1");
        i.setFrameData("string 2");
        i.setFrameData("string 3");
        i.setFrameData("string 4");
        i.setFrameData("string 5");
        i.setFrameData("string 6");
        i.setFrameData("string 7");
        i.setFrameData("string 8");
        i.setFrameData("string 9");
        i.setFrameData("string 10");
       
        Frame j = new Frame();
        j.setFrame_No(4);
        j.setFrameData("string 1");
        j.setFrameData("string 2");
        j.setFrameData("string 3");
        j.setFrameData("string 4");
       
        FrameQueue data =  new FrameQueue(5);
        data.enqueue(f);
        System.out.println(data.toString());
        data.enqueue(g);
        System.out.println(data.toString());
        data.enqueue(h);
        System.out.println(data.toString());
        data.enqueue(i);
        System.out.println(data.toString());
        data.enqueue(j);
        System.out.println(data.toString());
        data.toString();
        System.out.println(data.toString());
        data.dequeue();
        System.out.println(data.toString());
        data.dequeue();
        System.out.println(data.toString());
        
    }
    
}
