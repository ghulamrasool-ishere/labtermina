/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionnumberone;

/**
 *
 * @author G A M A
 */
public class Frame {
    private int frame_No;
    Stack FrameData = new Stack(10);

   

    public int getFrame_No() {
        return frame_No;
    }

    public Stack getFrameData() {
        return FrameData;
    }

    public void setFrame_No(int frame_No) {
        this.frame_No = frame_No;
    }

    public void setFrameData(String a) {
        FrameData.push(a);
    }
    public String toString(){
        String str = "Frame No : " + frame_No + "\nFrame Data : \n" + FrameData.toString();
        return str;
        
    }
    
}
